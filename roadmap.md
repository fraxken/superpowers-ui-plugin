# Superpowers UI roadmap :

## Version 0.0.5 (stable) :
- [x] Implémentation du positionnement hors flux.
- [x] Intégration de l'API de positionnement a la class Stylesheets.
- [x] Better logger system.

## Version 0.0.6 :
- [ ] Support du mouvement avec un gamepad dans l'interface.
- [ ] Widget input

## Plus tard :
- [ ] Implémentation des widgets [Window,Input,Select,Checkbox,etc..].
- [ ] Implémentation du drag'n'drop.
- [ ] Implémentation de la scrollbar.
- [ ] Implémentation des layouts.
- [ ] Remove DOM si possible.
