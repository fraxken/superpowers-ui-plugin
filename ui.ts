enum UIProperty {
  scale,
  opacity,
  euler,
  sprite,
  text,
  color,
  size,
  font
}

enum UIAction {
  click,
  rightclick,
  rightpressed,
  middleclick,
  hover,
  unhover,
  pressed,
  doubleclick,
  focus,
  unfocus
}

enum UIWidgets {
  input,
  select,
  checkbox,
  volume,
  button
}

enum Axis {
  X = 1,
  Y = 2,
  centerX = 3,
  centerY = 4
}

type UICoord = {position: string,decal?: number};

class UIBehavior extends Sup.Behavior {
  ray : Sup.Math.Ray;
  camera : Sup.Camera;

  static widgets : { [key:string] : (self: UIBehavior) => void } = {
    "default" : function(self : UIBehavior) : void {
      self.ray.setFromCamera(self.camera,Sup.Input.getMousePosition());
      const IT = self.ray.intersectActor(self.actor,false);

      if(IT.length != 0) {
        const o = IT[0];
        if(!self.isHover) {
          self.isHover = true;
          if(self["hover"] != null) {
            self["hover"](self);
          }
        }
        if(Sup.Input.wasMouseButtonJustPressed(0)) {
          if(!self.isFocus) {
            self.isFocus = true;
            if(self["focus"] != null) {
              self["focus"](self);
            }
          }
          self.clickNB++;

          window.clearTimeout(self.clickTimedout);
          self.clickTimedout = window.setTimeout(() =>{
            self.clickNB = 0;
            window.clearTimeout(self.clickTimedout);
          }, 400);

          if(self["click"] != null) {
            self["click"](self);
          }

          if(self["doubleclick"] != null && self.clickNB == 2) {
            self["doubleclick"](self);
            self.clickNB = 0;
          }

        }
        else if(Sup.Input.isMouseButtonDown(0) && self["pressed"] != null) {
          self["pressed"](self);
        }
        else if(Sup.Input.wasMouseButtonJustPressed(2) && self["rightclick"] != null) {
          self["rightclick"](self);
        }
        else if(Sup.Input.isMouseButtonDown(2) && self["rightpressed"] != null) {
          self["rightpressed"](self);
        }
        else if(Sup.Input.wasMouseButtonJustPressed(1) && self["middleclick"] != null) {
          self["middleclick"](self);
        }
      }
      else {
        if(self.isHover) {
          self.isHover = false;
          self.reset();
          if(self["unhover"] != null) {
            self["unhover"](self);
          }
        }
        if(self.isFocus) {
          self.isFocus = false;
          if(self["unfocus"] != null) {
            self["unfocus"](self);
          }
        }
      }
    },
    "input" : function(self: UIBehavior) : void {
      const inputkey : string = Sup.Input.getTextEntered();
      if(inputkey != "") {
        Sup.log(inputkey);
      }
    }
  }

  public isHover : boolean = false;
  public isFocus : boolean = false;
  public clickNB : number = 0;
  public prop : any = {}

  private widget : string = "default";
  private reseatable : boolean = true;
  private component : any = null;
  private textChild : Sup.Actor;
  private excepts_reset : any = {}
  private awakened : boolean = false;
  private clickTimedout : any = null;

  public awake() : void {
    if(!this.awakened) {
      this.awakened = true;
      this.camera = UI.camera.camera;
      this.ray = new Sup.Math.Ray(
        UI.camera.getPosition(),
        new Sup.Math.Vector3(0,0,-1)
      );

      for (const prop in UIProperty) {
        if (UIProperty.hasOwnProperty(prop) && (isNaN(parseInt(prop)))) {
          this.prop[prop] = null;
          this[prop] = null;
          this[prop] = <(self? : UIBehavior) => any>this[prop];
        }
      }
    }
  }

  public start() : void {
    // On récupère le composant (textRenderer ou spriteRenderer)
    this.component = UI.getComponents(this.actor);

    // Props configuration
    this.prop.scale = this.actor.getLocalScale();
    if(this.component != null) {
      this.prop.opacity = this.component.getOpacity();
    }
    this.prop.euler = this.actor.getLocalEulerAngles();

    if(this.actor.spriteRenderer != null) {
      this.prop.sprite = this.actor.spriteRenderer.getSprite();
    }

    this.textChild = this.actor.getChild("text");
    if(this.textChild != null) {
      this.prop.text = this.textChild.textRenderer.getText();
      this.prop.color = this.textChild.textRenderer.getColor();
      this.prop.font = this.textChild.textRenderer.getFont();
      this.prop.size = this.textChild.textRenderer.getSize();
    }

    if(this.widget == "input") {
      this["tampon"] = "";
      this["carac_min"] = 2;
      this["carac_max"] = 10;
    }
  }

  // On vérifie l'état d'exception en fonction du reseteable!
  public checkReset(property: string) : boolean {
    if(this.reseatable) {
      return (this.excepts_reset[property]) ? false : true;
    }
    return (this.excepts_reset[property]) ? true : false;
  }

  public reset() : void {

    if(this.checkReset("scale"))
      this.actor.setLocalScale(this.prop.scale);

    if(this.checkReset("opacity"))
      this.component.setOpacity(this.prop.opacity);

    if(this.checkReset("euler"))
      this.actor.setLocalEulerAngles(this.prop.euler);

    if(this.checkReset("sprite"))
      if(this.prop.sprite != null) {
        this.actor.spriteRenderer.setSprite(this.prop.sprite);
      }

    if(this.checkReset("text"))
      if(this.prop.text != null) {
        this.textChild.textRenderer.setText(this.prop.text);
        this.textChild.textRenderer.setColor(this.prop.color);
        this.textChild.textRenderer.setFont(this.prop.font);
        this.textChild.textRenderer.setSize(this.prop.size);
      }
  }

  public update() : void {
    UIBehavior.widgets[this.widget](this);
  }
}
Sup.registerBehavior(UIBehavior);

class UI {

  private o : Sup.Actor;

  static log : boolean = true;
  private static logHeader : string = "[UI-CLASS] => ";

  public static camera : Sup.Actor;
  public static font : string = null;

  private static screensize : {x:number,y:number} = Sup.Input.getScreenSize();
  private static sss : number;
  private static pixeltounits : number;

  private static storage : any = {};

  constructor(o : Sup.Actor) {
    if(o == null) UI.logger(UI.logHeader+`L'acteur => ${o} est introuvable dans votre scène.`,2)
    this.o = UI.checkBehavior(o);
  }

  public on(event: string, action: (self? : UIBehavior) => any) : void {
    if(UIAction[event] == undefined)
      UI.logger(UI.logHeader+`Evènement ${event} inconnu`,2);
    this.o.getBehavior(UIBehavior)[event] = action;
  }

  static init() : void {
    this.screensize = Sup.Input.getScreenSize();
    this.sss = this.screensize.y;
    this.pixeltounits = this.camera.camera.getOrthographicScale() / this.sss;
  }

  static logger(msg : string,level?:number) : void {
    // 0 : none
    // 1 : warning
    // 2 : critical
    level = level || 0;
    if(this.log) {
      switch(level) {
        case 0 :
          console.log(msg);
        break;
        case 1 :
          console.warn(msg);
        break;
        case 2 :
          throw new Error(msg);
        break;
      }
    }
  }

  static setCamera(camera : Sup.Actor) : void {
    if(camera == null)
      UI.logger(UI.logHeader+`L'acteur est caméra est introuvable`);
    if(camera.camera == null)
      UI.logger(UI.logHeader+`L'acteur caméra ne possède aucun composant 'Caméra'`);
    this.camera = camera;
  }

  static setFont(font : string) : void {
    this.font = font;
  }

  static getComponents(actor: Sup.Actor) : Sup.SpriteRenderer | Sup.TextRenderer {
    if(actor.spriteRenderer != null) return actor.spriteRenderer;
    if(actor.textRenderer != null) return actor.textRenderer;
    return null;
  }

  static checkBehavior(o : Sup.Actor) : Sup.Actor {
    if (o.getBehavior(UIBehavior) == null)
      o.addBehavior(UIBehavior);
    return o;
  }

  /* --- Containers méthodes --- */
  static active_container : string = null;
  static containers : { [key:string] : Sup.Actor } = {};
  static defineContainers(A : [Sup.Actor]) : void {
    let find = false;
    for(const actor in A) {
      let oActor = A[actor];
      if(oActor == null) {
        UI.logger(`Impossible de définir un container qui à pour valeur null.`,1);
      }
      else {
        if(!find) {
          find = true;
          this.active_container = oActor.getName();
          oActor.setLocalPosition(new Sup.Math.Vector3(0,0,-5));
        }
        else {
          oActor.setLocalPosition(new Sup.Math.Vector3(100,0,-5));
        }
        this.containers[oActor.getName()] = oActor;
      }
    }
  }

  static focus(containerName: string) : void {
    if(containerName != this.active_container) {
      let F = this.containers[containerName];
      if(this.containers[containerName] != undefined) {
        let old = this.containers[this.active_container];
        old.setLocalPosition(new Sup.Math.Vector3(100,0,-5));
        F.setLocalPosition(new Sup.Math.Vector3(0,0,-5));
        this.active_container = containerName;
      }
      else {
        UI.logger(`Le container ${containerName} est introuvable dans le registre des containers!`,1);
      }
    }
  }

  /* Absolute position */
  static setPosition(object:Sup.Actor,x:UICoord,y:UICoord,autodecal?:boolean) : void {
    autodecal = autodecal || true;
    x.decal = x.decal || 0;
    y.decal = y.decal || 0;

    let spriteSize = {x:0,y:0};
    if(autodecal) {
      let sprite = object.spriteRenderer.getSprite();
      let spritePTU = sprite.getPixelsPerUnit();
      let size = sprite.getGridSize();
      size.width = size.width / spritePTU;
      size.height = size.height / spritePTU;

      spriteSize = {
        x : size.width / 2,
        y : size.height / 2
      }
    }

    let Lx : number = (x.position == "left") ? -(this.screensize.x / 2) * this.pixeltounits + (x.decal + spriteSize.x):
        ( (x.position == "right") ? (this.screensize.x / 2) * this.pixeltounits - (x.decal + spriteSize.x) : 0 );
    let Ly : number = (y.position == "bottom") ? -(this.screensize.y / 2) * this.pixeltounits + (y.decal + spriteSize.y):
        ( (y.position == "top") ? (this.screensize.y / 2) * this.pixeltounits - (y.decal + spriteSize.y) : 0 );

    let name = object.getName();
    let store = UI.storage[name] = {
      object : object,
      x : x,
      y : y,
      autodecal : autodecal
    }

    object.setPosition( new Sup.Math.Vector3(Lx,Ly,-5) );
  }

  static update() : void {
    let tss = Sup.Input.getScreenSize();

    if(this.screensize.x != tss.x || this.screensize.y != tss.y) {
      this.screensize = Sup.Input.getScreenSize();
      this.sss = this.screensize.y;
      this.pixeltounits = this.camera.camera.getOrthographicScale() / this.sss;

      for(const index in this.storage) {
        const v = this.storage[index];
        UI.setPosition( v.object , v.x , v.y , v.autodecal );
      }
    }

  }
}

class Stylesheets  {

  private static warningHeader : string = "[Stylesheets-Compilateur] => ";

  private T : any = null;
  private static importantTriggers : string[] = ["sprite","reset"];

  private eventTrigger = (action: string,v: () => void | {},actor: Sup.Actor,depth: number) : void => {

    // On récupère le behavior correct!
    let behavior = actor.getBehavior(UIBehavior);

    if(typeof v == "function")
      behavior[action] = v;
    else {
      let T_Callback = {};
      for(const actionName in v) {

        if(UIAction[actionName] != undefined) {
          throw new Error(Stylesheets.warningHeader+`Evènement ${actionName} impossible en profondeur +0`);
        }

        if(Stylesheets.actionTriggers[actionName] != undefined) {
          T_Callback[actionName] = Stylesheets.actionTriggers[actionName];
        }
        else {
          UI.logger(Stylesheets.warningHeader+`L'action ${actionName} est introuvable dans le registre des triggers CSS!`,1);
        }
      }
      behavior[action] = () => {
        for(let i in T_Callback) {
          T_Callback[i](i,v[i],actor,1);
        }
      }
    }
  }

  private static actionTriggers : any = {
    scale : (actionName: string,v: Sup.Math.Vector3 | number,actor: Sup.Actor,depth: number) : void => {
      actor.setLocalScale(<any>v);
      if(depth == 0) actor.getBehavior(UIBehavior)["prop"]["scale"] = v;
    },
    component : (actionName: string,v: string,actor: Sup.Actor,depth: number) : void => {

    },
    opacity : (actionName: string,v: number | string,actor: Sup.Actor,depth: number) : void => {
      let comp = UI.getComponents(actor);
      if(comp != null) {
        comp.setOpacity(<any>v);
        if(depth == 0) actor.getBehavior(UIBehavior)["prop"]["opacity"] = v;
      }
    },
    text : (actionName: string,v: any,actor: Sup.Actor,depth: number) : void => {
      let text : Sup.Actor = actor.getChild("text");
      if(text == null) {
        text = new Sup.Actor("text",actor).setLocalPosition( new Sup.Math.Vector3(0,0,0.5) );
        let textComp : Sup.TextRenderer;
        if(typeof v == "object") {
          v.text = v.text || "undefined";
          v.font = v.font || (UI.font != null) ? UI.font : null;

          textComp = new Sup.TextRenderer(text,v.text);
          if(v.font != null) {
            textComp.setFont(v.font);
          }
          if(v.color != undefined) {
            textComp.setColor(v.color);
          }
          if(v.size != undefined) {
            textComp.setSize(v.size);
          }
        }
        else {
          textComp = new Sup.TextRenderer(text,<any>v);
          actor.getBehavior(UIBehavior)["prop"]["text"] = <any>v;
          if(UI.font != null) textComp.setFont(UI.font);
        }
      }
      else {
        text.textRenderer.setText(<any>v);
        actor.getBehavior(UIBehavior)["prop"]["text"] = <any>v;
      }
    },
    font : (actionName: string,v: string,actor: Sup.Actor,depth: number) : void => {
      let text : Sup.Actor = actor.getChild("text");
      (text != null) ? text.textRenderer.setFont(v) : UI.logger(Stylesheets.warningHeader+`L'enfant "text" n'est pas disponible sur l'acteur => ${actor.getName()}`,1);
    },
    color : (actionName: string,v: Sup.Color,actor: Sup.Actor,depth: number) : void => {
      let text : Sup.Actor = actor.getChild("text");
      (text != null) ? text.textRenderer.setColor(v) : UI.logger(Stylesheets.warningHeader+`L'enfant "text" n'est pas disponible sur l'acteur => ${actor.getName()}`,1);
    },
    size : (actionName: string,v: number,actor: Sup.Actor,depth: number) : void => {
      let text : Sup.Actor = actor.getChild("text");
      (text != null) ? text.textRenderer.setSize(v) : UI.logger(Stylesheets.warningHeader+`L'enfant "text" n'est pas disponible sur l'acteur => ${actor.getName()}`,1);
    },
    sprite : (actionName: string,v: string,actor: Sup.Actor,depth: number) : void => {
      (actor.spriteRenderer != null) ? actor.spriteRenderer.setSprite(v) : new Sup.SpriteRenderer(actor,v);
      if(depth == 0) actor.getBehavior(UIBehavior)["prop"]["sprite"] = v;
    },
    exec : (actionName: string,v: (actor? : Sup.Actor) => any,actor: Sup.Actor,depth: number) : any => {
      (depth > 0) ? v(actor) : UI.logger("Exec est une action de profondeur 1+",1);
    },
    url : (actionName: string,v: string,actor: Sup.Actor,depth: number) : any => {
      let win = window.open(v, '_blank');
      win.focus();
    },
    exit : (actionName: string,v: () => void,actor: Sup.Actor,depth: number) : any => {
      v();
      Sup.exit();
    },
    fullscreen : (actionName: string,v: boolean, actor: Sup.Actor,depth: number) : any => {
      if(depth >0) (v) ? Sup.Input.goFullscreen() : Sup.Input.exitFullscreen();
      else UI.logger(Stylesheets.warningHeader+`La profondeur de fullscreen est de 1+`,1);
    },
    excepts : (actionName: string,v: string[],actor: Sup.Actor,depth: number) : any => {
      if(depth == 0) {
        for(let i in v) {
          if(UIProperty[v[i]]) {
            actor.getBehavior(UIBehavior)["excepts_reset"][v[i]] = true;
          }
          else {
            UI.logger(Stylesheets.warningHeader+`La propriété d'exception ${i} n'est pas valide!`,1);
          }
        }
      }
      else {
        UI.logger("Excepts n'est pas une action de profondeur +0",1);
      }
    },
    loadScene : (actionName: string,v: string,actor: Sup.Actor,depth: number) : any => {
      if(depth > 0) Sup.loadScene(v);
      else UI.logger(Stylesheets.warningHeader+`La profondeur de loadScene est de 1+`,1);
    },
    position : (actionName: string,v: any,actor: Sup.Actor,depth: number) : any => {
      if(depth == 0) {
        UI.setPosition(actor,v.x,v.y,v.autodecal);
      }
    },
    setContainer : (actionName: string,v: string,actor: Sup.Actor,depth: number) : any => {
      if(depth > 0) UI.focus(v);
      else UI.logger(Stylesheets.warningHeader+`La profondeur de focus est de 1+`,1);
    },
    mouse : (actionName: string,v: boolean,actor: Sup.Actor,depth: number) : any => {
      if(depth > 0) Sup.Input.setMouseVisible(v);
      else UI.logger(Stylesheets.warningHeader+`La profondeur de mouse est de 1+`,1);
    },
    destroy : (actionName: string,v: any,actor: Sup.Actor,depth: number) : any => {
      if(depth > 0) {
        if(typeof v == "function") v();
        actor.destroy();
      }
      else {
        UI.logger(Stylesheets.warningHeader+`La profondeur de destroy est de 1+`,1);
      }
    },
    visible : (actionName: string,v: boolean,actor: Sup.Actor,depth: number) : void => {
      actor.setVisible(v);
    },
    reset : (actionName: string,v: boolean,actor: Sup.Actor,depth: number) : void => {
      actor.getBehavior(UIBehavior)["reseatable"] = v;
    }
  }

  constructor(callback : () => {}) {
    this.T = callback();
    Object.keys(UIAction)
      .filter(v => isNaN(parseInt(v, 10)))
      .forEach(v => Stylesheets.actionTriggers[v] = this.eventTrigger);
  }

  public load() : void {
    for(const actor in this.T) {
      // On vérifie l'object actor! (comme ça on peut crée un multiple selector!)

      // Application de la méthodologie sur actor.
      let oActor = Sup.getActor(actor);
      UI.checkBehavior(oActor);
      if(oActor != null) {
        if(this.T[actor].length != 0)
          this.forEachAction(this.T[actor],oActor,0);
      }
      else {
        UI.logger(Stylesheets.warningHeader+`L'acteur ${actor} est introuvable!`,1);
      }
    }
  }

  private merge(o: any,source: any) : any {
    for(const i in o) {
      let env : any = o[i];
      if(typeof env == "object") {
        for(let j in env) {
          source[j] = env[j];
        }
      }
    }
    return source;
  }

  private forEachAction(actionArray : any,actor: Sup.Actor,depth: number) : void {

    // On récupère le behavior correct!
    let behavior = actor.getBehavior(UIBehavior);

    // On détecte si un extend est présent!
    if(actionArray.extends != null) {
      actionArray = this.merge(actionArray.extends,actionArray);
      actionArray.extends = null;
      delete actionArray.extends;
    }

    if(actionArray.widget != null) {
      behavior["widget"] = actionArray.widget;
      actionArray.widget = null;
      delete actionArray.widget;
    }

    if(actionArray.sprite != null) {
      (actor.spriteRenderer != null) ? actor.spriteRenderer.setSprite(actionArray.sprite) : new Sup.SpriteRenderer(actor,actionArray.sprite);
      if(depth == 0) actor.getBehavior(UIBehavior)["prop"]["sprite"] = actionArray.sprite;
      actionArray.sprite = null;
      delete actionArray.sprite;
    }

    let XPos : UICoord;
    let YPos : UICoord;

    if(actionArray.left != undefined) {
      XPos = {
        position : "left",
        decal : actionArray.left
      }
      actionArray.left = null;
      delete actionArray.left;
    }
    else if(actionArray.right != undefined) {
      XPos = {
        position : "right",
        decal : actionArray.right
      }
      actionArray.right = null;
      delete actionArray.right;
    }
    else if(actionArray.centerx != undefined) {
      XPos = {
        position : "center",
        decal : actionArray.centerx
      }
      actionArray.centerx = null;
      delete actionArray.centerx;
    }

    if(actionArray.top != undefined) {
      YPos = {
        position : "top",
        decal : actionArray.top
      }
      actionArray.top = null;
      delete actionArray.top;
    }
    else if(actionArray.bottom != undefined) {
      YPos = {
        position : "bottom",
        decal : actionArray.bottom
      }
      actionArray.bottom = null;
      delete actionArray.bottom;
    }
    else if(actionArray.centery != undefined) {
      YPos = {
        position : "center",
        decal : actionArray.centery
      }
      actionArray.centery = null,
      delete actionArray.centery;
    }

    actionArray.position = {
      x : XPos,
      y : YPos,
      autodecal : actionArray.autodecal || true
    }


    // Do it!
    for(let actionName in actionArray) {
      actionName.toLowerCase();
      if(depth != 0)
        if(UIAction[actionName] != undefined) UI.logger(Stylesheets.warningHeader+`Evènement ${actionName} impossible en profondeur +0`,2)
      if(Stylesheets.actionTriggers[actionName] != undefined)
        Stylesheets.actionTriggers[actionName](actionName,actionArray[actionName],actor,depth,behavior);
      else
        UI.logger(Stylesheets.warningHeader+`L'action ${actionName} est introuvable dans le registre des triggers CSS!`,1);
    }
  }
}
