# Superpowers UI v0.0.5

A **class set for making user-interface** in superpowers with components like spriteRenderer /
textRenderer.

**The DOM plugin is required**. Download it [here](https://github.com/florentpoujol/superpowers-dom-plugin)


## Installation

Add UI.ts in your project (at **the top** of all of your **game behavior scripts**).

> A plugin version incomming soon.

## First step

In one of your game behavior add these lines in **awake** :

```javascript
class GameBehavior extends Sup.Behavior {
  awake() {
    UI.setCamera(Sup.getActor("camera"));
    UI.setFont("path to font");
    UI.init();
    UI.defineContainers([
      Sup.getActor("container_accueil"),
      Sup.getActor("container_option")
    ]);
  }
}
Sup.registerBehavior(GameBehavior);
```

## Make a stylesheet

> A CSS Like for making user-interface more easily.

```javascript
let CSSMaster = new Stylesheets(function() {
  let CSS : any = {};

  // menu is an actor in your scene.
  CSS.menu = {
    click : function() {
      Sup.log("Hello world!");
    }
  }

  return CSS;
});
```

More about stylesheet in the documentation [here](https://google.fr)

## Add the stylesheet in your behavior

```javascript
...
awake() {
    UI.setCamera(Sup.getActor("camera"));
    UI.setFont("path to font");
    UI.init();
    UI.defineContainers([
      Sup.getActor("container_accueil"),
      Sup.getActor("container_option")
    ]);
    CSSMaster.load();
}
...
```
#### > Enjoy !
## Warning

Window is declared in UI.ts
```javascript
declare var window : Window;
```

## Need help / a contribution ?

- ** Twitter ** : https://twitter.com/fraxken
