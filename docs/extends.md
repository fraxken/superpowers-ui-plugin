# Stylesheet extend

When you have three menu with the same code, the extends command inc to make your life better !

#### Exemple :

```javascript
let CSSMaster = new Stylesheets(function() {
  let CSS : any = {};


  let color = new Sup.Color(33,150,150);
  let color_hover = new Sup.Color(233,30,99);

  let hoverAction = {
    color : color,
    hover : {
      sprite : "assets/sprites/menu_hover",
      color : color_hover
    },
    unhover : {
      color : color
    }
  }

  CSS.menu_play = {
    text : "Play",
    click : {loadScene : "scenes/jeu"},
    extends : [hoverAction]
  }

  CSS.menu_option = {
    text : "Option",
    click : {setContainer : "option"},
    extends : [hoverAction]
  }

  CSS.menu_back = {
    text : "Back",
    click : {setContainer : "accueil"},
    extends : [hoverAction]
  }

  return CSS;
});
```
