# UI - positionnement

Table of value (for UI Class) :

| Value | axis |
| --- | --- |
| left | X |
| right | X |
| center | X & Y |
| top | Y |
| bottom | Y |

> Autodecal : Add the width/height (/2) of the spriteRenderer.
>
> Decal : Number of decal (units)

### Set the position with UI Class :

```javascript
awake() {
    // UI.setPosition(actor,x,y,autodecal)
    UI.setPosition(Sup.getActor("menu"),{"left",0},{"top",0},true);
}
```

![Positionnement](./img/menuabsolute.png)

### Set the position with a stylesheet :

```javascript
CSS.Menu = {
    left : 0, // right - centerx
    top : 0 // bottom - centery
}
```
