# Button action commands

* click
* pressed
* doubleclick
* rightclick
* rightpressed
* hover
* unhover
* focus
* unfocus

> Behavior is auto created when UI Class or Stylesheet is called!

## With UI Class

```javascript
awake() {
    let menu = new UI(Sup.getActor("menu"));
    menu.on("click",function() {
        Sup.log("Hello world!");
    });
}
```

## With stylesheet :

##### Exemple :

> The action returns a function or an array of sub-commands. Be careful to the depth
of each command.

```javascript
CSS.Menu = {
    scale : 1,
    click = {
        opacity : 0.5
    },
    hover = {
        scale : 1.2
    }
}
```

Other exemple with function

```javascript
CSS.Menu = {
    scale : 1,
    click = function(self : UIBehavior) {
        Sup.log(self.actor.getName()+" clicked!");
    }
}
```

##### Button action and automatic reset :

Take the lastest exemple **and see what Stylesheet make for you** :

```javascript
CSS.Menu = {
    reset : true,
    scale : 1,
    click = {
        opacity : 0.5
    },
    hover = {
        scale : 1.2
    },
    unhover = {
        scale : 1
    },
    unfocus = {
        opacity : 1 // Default opacity of our object.
    }
}
```

set **reset to false** if you want to **disabled automatic reseting**.

#### Scalable reset

In this exemple, i made the choice to turn off only the scale reset. The concept work with
reset : true and false.

```javascript
CSS.Menu = {
    scale : 1,
    excepts : ["scale"], // ["scale","opacity","sprite","etc.."]
    click = {
        opacity : 0.5
    },
    hover = {
        scale : 1.2
    }
}
```

#### Exec command

Exec is very useful when you make a table of command but you need to make some thing else when the action is triggered.

```javascript
CSS.Menu = {
    click = {
        opacity : 0.5,
        exec : function(actor) {
            Sup.log(actor.getName()+" clicked!");
        }
    }
}
```
