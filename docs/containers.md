# UI - Containers

Containers are actors groups and they mostly serve to move sets of actor very easily.

## Add containers in your gamecore :

```javascript
UI.defineContainers([
  Sup.getActor("container_accueil"),
  Sup.getActor("container_option")
]);
```

## Focus with UI Class

```javascript
UI.focus("container_option");
```

## Focus with Stylesheet

```javascript
CSS.Menuoption = {
    click : {
        setContainer : "container_option"
    }
}
```
