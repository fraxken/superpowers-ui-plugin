# Stylesheets

* All position commands are [here](./docs/position.md)
* Button actions are [here](./docs/button_actions.md)
* Extends [here](./docs/extends.md)

# Depth

```javascript
CSS.Menu = {
    reset = false, // Depth = 0
    onclick = {
        opacity : 1 // Depth = 1
    }
}
```

## Important commands :

| Name | Description | Depth | Type |
| --- | --- | --- | --- |
| opacity | - | all | number
| scale | - | all | vector3 or number
| sprite | - | all | string
| extends | - | 0 | table of object
| excepts | - | 0 | table of string
| exec | - | 1 | function
| loadScene | - | 1 | string
| setContainer | - | 1 | string
| reset | - | 0 | boolean

## TextRenderer commands :

**warning** : Set a global font with UI.setFont("path");

> text command create automatically a textRenderer component.

| Name | Description | Depth | Type |
| --- | --- | --- | --- |
| text | - | all | string or object
| font | - | all | string
| color | - | all | Sup.color
| size | - | all | number

## Other commands :

| Name | Description | Depth | Type |
| --- | --- | --- | --- |
| visible (actor) | - | all | boolean
| mouse (visible) | - | 1 | boolean
| destroy (actor) | - | 1 | any or function
| exit | - | 1 | number or function
| fullscreen | - | 1 | boolean
